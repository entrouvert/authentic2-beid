from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('',
    url(r'^signin$',
        handle_authentication,
        name='beid_signin'),
    url(r'^add', add_beid, name='add_beid'),
    url(r'^delete/(?P<certificate_pk>\d+)/$',
        delete_beid, name='delete_beid'),
    url(r'^activate/(?P<registration_token>[\w:-]+)/$',
        registration_completion, name='beid_registration_activate'),
    url(r'^x509/(?P<registration_token>[\w:-]+)/$', x509, name='x509'),
)
