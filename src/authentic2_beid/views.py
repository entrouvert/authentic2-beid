import logging

from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from authentic2.auth2_auth.auth2_ssl import models
from authentic2.utils import continue_to_next_url, redirect, redirect_to_login

from authentic2.registration_backend.views import valid_token, RegistrationCompletionView

from .backends import BeIDBackend
from .util import SSLInfo, get_x509_url, get_user_names, set_rrn

logger = logging.getLogger(__name__)


def handle_authentication(request, *args, **kwargs):
    ssl_info  = SSLInfo(request)
    logger.debug('received SSL info: %s', ssl_info)
    if not request.user.is_authenticated():
        user = authenticate(ssl_info=ssl_info)
        logger.debug('got user: %s', user)
        if user:
            login(request, user)
        else:
            messages.error(request, _('Authentication with your BeID card failed.'
                'Please make sure your eID card is linked to your account and is plugged in.'
            ))
            return redirect_to_login(request)

    return continue_to_next_url(request)


def add_beid(request):
    if request.user.is_authenticated:
        ssl_info  = SSLInfo(request)
        if BeIDBackend().link_user(ssl_info, request.user):
            set_rrn(request.user, ssl_info)
            logger.info('Successful linking of the SSL '
               'certificate to an account')
            messages.info(request, _('BeID card successfully linked to your account'))
        else:
            logger.error('eid linking failed')
            messages.error(request, _('BeID linking failed. Internal server error.'))

    return redirect(request, 'account_management',
                    fragment='a2-beid-certificate-profile')


def profile(request, template_name='beid/profile.html', *args, **kwargs):
    context_instance = kwargs.pop('context_instance', None) or \
        RequestContext(request)
    certificates = models.ClientCertificate.objects.filter(user=request.user)
    ctx = {'certificates': certificates}
    return render_to_string(template_name, ctx,
            context_instance=context_instance)

def delete_beid(request, certificate_pk):
    try:
        beid = models.ClientCertificate.objects.get(pk=certificate_pk)
        beid.delete()
        logger.info('client certificate %s deleted', beid)
        messages.info(request, _('Your BeID card informations are successfully deleted'))
    except models.ClientCertificate.DoesNotExist:
        logger.info('no client certificate %s', certificate_pk)
        messages.error(request, _('No BeID card associated to this account'))
    return redirect(request, 'account_management',
            fragment='a2-beid-certificate-profile')

def x509(request, registration_token):
    ssl_info = SSLInfo(request)
    if ssl_info.get('subject_dn'):
        request.session['ssl_info'] = ssl_info.__dict__
    return redirect(request, 'beid_registration_activate',
                    kwargs={'registration_token': registration_token})

def beid_activate(request, registration_token):
    return HttpResponseRedirect(get_x509_url(request) + reverse('x509',
                        kwargs={'registration_token': registration_token}))



class BeIDRegistrationCompletionView(RegistrationCompletionView):

    def get_form_kwargs(self, **kwargs):
        kw = super(BeIDRegistrationCompletionView, self).get_form_kwargs(**kwargs)
        # get first and last names from certificate and fill the form
        if self.request.session.get('ssl_info'):
            f_name, l_name = get_user_names(self.request.session['ssl_info'])
            kw['initial'].update({'first_name': f_name, 'last_name': l_name})
        return kw

    def form_valid(self, form):
        ret = super(BeIDRegistrationCompletionView, self).form_valid(form)
        if self.request.session.get('ssl_info'):
            set_rrn(self.object, self.request.session['ssl_info'])
            cert = models.ClientCertificate(user=self.object)
            cert.__dict__.update(self.request.session['ssl_info'])
            cert.save()
        return ret

registration_completion = valid_token(BeIDRegistrationCompletionView.as_view())
