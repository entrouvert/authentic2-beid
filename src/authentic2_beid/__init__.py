__version__ = '0.0.1'

class Plugin(object):

    def get_before_urls(self):
        from django.conf.urls import url, patterns, include
        return patterns('',
                        url('accounts/', include(__name__ + '.registration_urls')),
                        url('accounts/beid/', include(__name__ + '.urls'))
        )

    def get_apps(self):
        return [__name__]

    def get_authentication_backends(self):
        return ('authentic2_beid.backends.BeIDBackend',)


    def get_auth_frontends(self):
        return ('authentic2_beid.frontends.BeIDFrontend',)

    def get_after_middleware(self):
        return ('authentic2_beid.middleware.SSLAuthMiddleware',)

    def get_apps(self):
        return ('authentic2.auth2_auth.auth2_ssl',
                'authentic2_beid')
