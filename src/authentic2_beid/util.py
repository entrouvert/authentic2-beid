from authentic2.models import Attribute

from authentic2.auth2_auth.auth2_ssl.util import SSLInfo as BaseSSLInfo
from authentic2.auth2_auth.auth2_ssl.util import explode_dn

from . import app_settings


class SSLInfo(BaseSSLInfo):

    def __init__(self, request):
        ssl_headers = [(k[5:], v) for k, v in request.META.iteritems() if k.startswith('HTTP_SS')]
        self.read_env(dict(ssl_headers))

def get_x509_url(request):
    return 'https://%s:%s' % (request.get_host().split(':')[0],
                    app_settings.AUTH_PORT)

def get_dn(ssl_info):
    dn = ssl_info.get('subject_dn')
    return dict(explode_dn(dn))

def get_user_names(ssl_info):
    dn = get_dn(ssl_info)
    names = dn['GN'].split(' ', 1)
    return names[0], dn['SN']

def get_rrn(ssl_info):
    dn = get_dn(ssl_info)
    return dn['serialNumber']

def set_rrn(user, ssl_info):
    try:
        rrn = Attribute.objects.get(name='rrn')
        rrn.set_value(user, get_rrn(ssl_info))
    except Attribute.DoesNotExist:
        pass
