from authentic2.auth2_auth.auth2_ssl.backends import SSLBackend
from authentic2.auth2_auth.auth2_ssl import util


class BeIDBackend(SSLBackend):

    def build_username(ssl_info):
        dn = dict(util.explode_dn(ssl_info.subject_dn))
        return dn.get('serialNumber')
