from django.utils.translation import gettext_noop
from django.contrib.auth import authenticate
from django.shortcuts import render
import django.forms

from authentic2.utils import redirect_to_login

from . import views

class BeIDFrontend(object):

    def enabled(self):
        return True

    def id(self):
        return 'eid'

    def name(self):
        return gettext_noop('Belgian eID card')

    def post(self, request, form, nonce, next_url):
        return redirect_to_login(request, login_url='beid_signin',)

    def form(self):
        return django.forms.Form

    def template(self):
        return 'beid/login_form.html'

    def profile(self, request, *args, **kwargs):
        return views.profile(request, *args, **kwargs)
