from django.contrib.auth import authenticate, login


from . import util, app_settings


class SSLAuthMiddleware(object):
    """
    attemps to login user based in client certificate info
    """
    def process_request(self, request):
        if request.user.is_authenticated():
            return
        ssl_info  = util.SSLInfo(request)
        user = authenticate(ssl_info=ssl_info)
        if user and request.user != user:
            login(request, user)
