from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('',
    url(r'^activate/(?P<registration_token>[a-zA-Z0-9:_-]+)/$',
        beid_activate, name='beid_activate'),
)
