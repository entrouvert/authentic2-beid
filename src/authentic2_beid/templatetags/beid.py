from django import template

from authentic2_beid.util import get_x509_url

register = template.Library()

@register.simple_tag
def beid_url(request):
    print "R: %s" % request
    return get_x509_url(request)
