#!/usr/bin/python
import sys
import os
import subprocess

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command

class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management.commands.compilemessages import \
                    compile_messages
            for path, dirs, files in os.walk('src'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                compile_messages(sys.stderr)
                os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print
            print


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            return result.split()[0][1:].replace('-', '.')
        else:
            return '0.0.0-%s' % len(
                    subprocess.check_output(
                            ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0.0'


README = file(os.path.join(
    os.path.dirname(__file__),
    'README')).read()

setup(name='authentic2-beid',
        version=get_version(),
        license='AGPLv3',
        description='Authentic2 Belgian eID authentication',
        long_description=README,
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages('src'),
        package_dir={
            '': 'src',
        },
        include_package_data=True,
        install_requires=[
            'authentic2',
        ],
        entry_points={
            'authentic2.plugin': [
                'authentic2-beid = authentic2_beid:Plugin',
            ],
        },
        cmdclass={
            'build': build,
            'install_lib': install_lib,
            'compile_translations': compile_translations,
            'sdist': eo_sdist},
)
